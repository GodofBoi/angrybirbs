﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingTarget : MonoBehaviour
{
    [Range( 0, 5 )]
    public float HalfPathDistance = 3;
    [Range( 0, 5 )]
    public float MovementSpeed = 2;

    private Rigidbody2D rb;

    public float startPosY;

    float direction = 1;

    private void Start()
    {
        //Get the component for rigibody2D
        rb = GetComponent<Rigidbody2D>();

        //Make the start position the position of the rigidbody's y on startup
        startPosY = rb.transform.position.y;
    }

    private void FixedUpdate()
    {
        rb.isKinematic = true;

        Vector2 directionVec = new Vector2(0, direction);

        //Controls the targets rigidbody
        rb.MovePosition(rb.position + directionVec * MovementSpeed * Time.deltaTime);

        //If the target reaches the max height change direction to down
        if (rb.position.y >= startPosY + HalfPathDistance)
        {
            direction = -1;
        }

        //else if the target reaches the lowest height change direction to up
        else if (rb.position.y <= startPosY - HalfPathDistance)

            direction = 1;

        //print(direction);
    }
}
