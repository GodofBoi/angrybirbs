﻿using UnityEngine;

public class AirSpecialBounce : MonoBehaviour, IAirSpecial
{
    [Range( 0, 1 )]
    public float SlowDownFactor = 1;

    private Rigidbody2D rb;

    public void ExecuteAirSpecial()
    {
        rb = GetComponent<Rigidbody2D>();

        //changes the velocity of the birb on click, slows it down by slow down factor
        rb.velocity = new Vector2(rb.velocity.x, Mathf.Abs(rb.velocity.y)) * (1 -SlowDownFactor);
    }
}
