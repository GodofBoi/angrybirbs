﻿using UnityEngine;

public class AirSpecialExplode : MonoBehaviour, IAirSpecial
{
    [Range( 0, 5 )]
    public float BlastRadius = 2;

    //Create a layermask
    [SerializeField]
    private LayerMask layerMask;

    public void ExecuteAirSpecial()
    {
        //Get the layer of the target
        LayerMask Target = LayerMask.GetMask("Target");

        //Create a list of colliders that create a circle around the game object, has a radius of the blastradius, shares the target layer
        Collider2D[] colliders = Physics2D.OverlapCircleAll(new Vector2(gameObject.transform.position.x, gameObject.transform.position.y), BlastRadius, Target);

        //For each collider in the colliders list destroy target, finds the target collisions and destroys them
        foreach (Collider2D col in colliders)
        {
            Destroy(col.gameObject);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, BlastRadius);
        Gizmos.color = Color.red;
    }
}
