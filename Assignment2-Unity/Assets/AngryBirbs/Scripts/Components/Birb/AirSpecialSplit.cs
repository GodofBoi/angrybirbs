﻿using UnityEngine;

public class AirSpecialSplit : MonoBehaviour, IAirSpecial
{
    public float SplitAngleInDegrees = 10;

    public void ExecuteAirSpecial()
    {
        //gets the game object of the clones
        GameObject birbClone1 = Birb.MakeBirbCopy(gameObject);
        GameObject birbClone2 = Birb.MakeBirbCopy(gameObject);

        //grabs the rigidbodies of the birbs
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        Rigidbody2D rbBirbClone1 = birbClone1.GetComponent<Rigidbody2D>();
        Rigidbody2D rbBirbClone2 = birbClone2.GetComponent<Rigidbody2D>();

        //Calculate angle of original birb
        float angle = Mathf.Atan2(rb.velocity.x, rb.velocity.y);

        //Calculate angle of birb that travels up
        float birbAngleUp = angle + (SplitAngleInDegrees * Mathf.Deg2Rad);

        //simiulate birb clones so they move
        rbBirbClone1.simulated = true;
        rbBirbClone2.simulated = true;

        //Calculate new direction for birb that travels up
        Vector2 newDirection = new Vector2(Mathf.Sin(birbAngleUp), Mathf.Cos(birbAngleUp));

        //Add force to birb that travels up
        rbBirbClone1.AddForce (newDirection.normalized * rb.velocity.magnitude, ForceMode2D.Impulse);

        //Calculate angle of birb that travels down
        float birbAngleDown = angle - (SplitAngleInDegrees * Mathf.Deg2Rad);

        //Calculate direction of birb thattravels down
        Vector2 newDirection2 = new Vector2(Mathf.Sin(birbAngleDown), Mathf.Cos(birbAngleDown));
        
        //Add force to birb that travels down
        rbBirbClone2.AddForce(newDirection2.normalized * rb.velocity.magnitude, ForceMode2D.Impulse);

    }
}
